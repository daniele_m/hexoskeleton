import re
import sys
from struct import pack, unpack
from rich.panel import Panel
from rich.columns import Columns
from rich.table import Table
from rich.padding import Padding
from rich.markup import escape
from rich import print, box
from capstone import Cs, CS_ARCH_X86, CS_MODE_32, CS_MODE_64
from unicorn import Uc, UcError, UC_HOOK_CODE, UC_ARCH_X86, UC_MODE_32, UC_MODE_64
from unicorn.x86_const import *

#Capstone options
CAP_ARCH = CS_ARCH_X86
CAP_MODE = CS_MODE_32

#Unicorn options
UNI_ARCH = UC_ARCH_X86
UNI_MODE_32 = UC_MODE_32
UNI_MODE_64 = UC_MODE_64
STACK_SIZE = 0x10000
HEAP_SIZE = 0x10000

#Global variables
GO = False

def get_registers_32(mu):
    registers = {
            "[dodger_blue1]ESP[/dodger_blue1]": hex(mu.reg_read(UC_X86_REG_ESP)),
            "[medium_purple]EBP[/medium_purple]": hex(mu.reg_read(UC_X86_REG_EBP)),
            "[red]EAX[/red]": hex(mu.reg_read(UC_X86_REG_EAX)),
            "[deep_pink3]EBX[/deep_pink3]": hex(mu.reg_read(UC_X86_REG_EBX)),
            "[dark_turquoise]ECX[/dark_turquoise]": hex(mu.reg_read(UC_X86_REG_ECX)),
            "[green_yellow]EDX[/green_yellow]": hex(mu.reg_read(UC_X86_REG_EDX)),
            "[sea_green3]ESI[/sea_green3]": hex(mu.reg_read(UC_X86_REG_ESI)),
            "[dark_orange3]EDI[/dark_orange3]": hex(mu.reg_read(UC_X86_REG_EDI)),
            "[grey50]EFLAGS[/grey50]": bin(mu.reg_read(UC_X86_REG_EFLAGS))
    }
    table = Table(box=box.ROUNDED) #title="Registers snapshot"
    table.add_column("Register")
    table.add_column("Value")
    for key, value in registers.items():
        table.add_row(key,value)
    return table

def get_registers_64(mu):
    registers = {
            "[dodger_blue1]RSP[/dodger_blue1]": hex(mu.reg_read(UC_X86_REG_RSP)),
            "[medium_purple]RBP[/medium_purple]": hex(mu.reg_read(UC_X86_REG_RBP)),
            "[red]RAX[/red]": hex(mu.reg_read(UC_X86_REG_RAX)),
            "[deep_pink3]RBX[/deep_pink3]": hex(mu.reg_read(UC_X86_REG_RBX)),
            "[dark_turquoise]RCX[/dark_turquoise]": hex(mu.reg_read(UC_X86_REG_RCX)),
            "[green_yellow]RDX[/green_yellow]": hex(mu.reg_read(UC_X86_REG_RDX)),
            "[sea_green3]RSI[/sea_green3]": hex(mu.reg_read(UC_X86_REG_RSI)),
            "[dark_orange3]RDI[/dark_orange3]": hex(mu.reg_read(UC_X86_REG_RDI)),
            "[white]R8 [/white]": hex(mu.reg_read(UC_X86_REG_R8)),
            "[white]R9 [/white]": hex(mu.reg_read(UC_X86_REG_R9)),
            "[white]R10[/white]": hex(mu.reg_read(UC_X86_REG_R10)),
            "[white]R11[/white]": hex(mu.reg_read(UC_X86_REG_R11)),
            "[white]R12[/white]": hex(mu.reg_read(UC_X86_REG_R12)),
            "[white]R13[/white]": hex(mu.reg_read(UC_X86_REG_R13)),
            "[white]R14[/white]": hex(mu.reg_read(UC_X86_REG_R14)),
            "[white]R15[/white]": hex(mu.reg_read(UC_X86_REG_R15)),
            "[grey50]EFLAGS[/grey50]": bin(mu.reg_read(UC_X86_REG_EFLAGS))
    }
    table = Table(box=box.ROUNDED) #title="Registers snapshot"
    table.add_column("Register")
    table.add_column("Value")
    for key, value in registers.items():
        table.add_row(key,value)
    return table

def get_esp(mu):
    esp = mu.reg_read(UC_X86_REG_ESP)
    i = esp
    rows = []
    while i < esp + 64:
        esp = mu.reg_read(UC_X86_REG_ESP)
        pesp = str(hex(i)) + ': '
        for b in mu.mem_read(i, 4)[::-1]:
            pesp += f'\\x{b:02x}'
        rows.append(pesp)
        i += 4
    #rows.reverse()
    table = Table(box=box.ROUNDED)
    table.add_column('Stack')
    for row in rows:
        table.add_row(row)
    return table

def get_rsp(mu):
    rsp = mu.reg_read(UC_X86_REG_RSP)
    i = rsp
    rows = []
    while i < rsp + 256:
        rsp = mu.reg_read(UC_X86_REG_RSP)
        prsp = str(hex(i)) + ': '
        for b in mu.mem_read(i, 8)[::-1]:
            prsp += f'\\x{b:02x}'
        rows.append(prsp)
        i += 8
    #rows.reverse()
    table = Table(box=box.ROUNDED)
    table.add_column('Stack')
    for row in rows:
        table.add_row(row)
    return table

def init_mu(mode):
    if mode == CS_MODE_32:
        mu = Uc(UNI_ARCH, UNI_MODE_32)
    else:
        mu = Uc(UNI_ARCH, UNI_MODE_64)
    stack = 0x30000000
    heap_base = 0x65000000
    mu.mem_map(stack, STACK_SIZE)
    mu.mem_map(heap_base, HEAP_SIZE)
    mu.mem_map(0, 0x1000)
    if mode == CS_MODE_32:
        mu.reg_write(UC_X86_REG_ESP, stack + STACK_SIZE - 4 - 0x200)
        mu.reg_write(UC_X86_REG_EBP, stack + STACK_SIZE - 4)
    else:
        mu.reg_write(UC_X86_REG_RSP, stack + STACK_SIZE - 8 - 0x200)
        mu.reg_write(UC_X86_REG_EBP, stack + STACK_SIZE - 8)
    return mu

def hook_code_32(mu, address, size, user_data):
    global GO
    eip = mu.reg_read(UC_X86_REG_EIP)
    eip = mu.mem_read(eip,size)      
    printable_eip = ''
    for b in eip: printable_eip += f'\\x{b:02x}'
    try:
        if eip != b'\x00\x00\x00\x00' and eip != b'\x00\x00':
            cap = Cs(CS_ARCH_X86, CS_MODE_32)
            disasm = cap.disasm(eip,size)
            for d in disasm:
                if GO == False:
                    title = f'EIP @ [#826FFF]{hex(address)}[/#826FFF] : [bold green]{d.mnemonic} {escape(d.op_str)}[/bold green] ([italic white]{printable_eip}[/italic white])'
                    print(
                        Panel(
                            Columns([get_registers_32(mu), get_esp(mu)], padding=(0,2)),
                            title=title,
                            title_align="left",
                            padding=(1,2)
                        )
                    )
                    x = input('Press <ENTER> to step forward or go to end by typing "g": ')
                    print('')
                    if 'g' in x: GO = True
        else:
            mu.emu_stop()
    
    except Exception as e:
        print(e)
    
    except KeyboardInterrupt:
        mu.emu_stop()
        print('[[red]![/red]] Emulation [red]stopped[/red]')

def get_registers_64(mu):
    registers = {
            "[dodger_blue1]RSP[/dodger_blue1]": hex(mu.reg_read(UC_X86_REG_RSP)),
            "[medium_purple]RBP[/medium_purple]": hex(mu.reg_read(UC_X86_REG_RBP)),
            "[red]RAX[/red]": hex(mu.reg_read(UC_X86_REG_RAX)),
            "[deep_pink3]RBX[/deep_pink3]": hex(mu.reg_read(UC_X86_REG_RBX)),
            "[dark_turquoise]RCX[/dark_turquoise]": hex(mu.reg_read(UC_X86_REG_RCX)),
            "[green_yellow]RDX[/green_yellow]": hex(mu.reg_read(UC_X86_REG_RDX)),
            "[sea_green3]RSI[/sea_green3]": hex(mu.reg_read(UC_X86_REG_RSI)),
            "[dark_orange3]RDI[/dark_orange3]": hex(mu.reg_read(UC_X86_REG_RDI)),
            "[white]R8 [/white]": hex(mu.reg_read(UC_X86_REG_R8)),
            "[white]R9 [/white]": hex(mu.reg_read(UC_X86_REG_R9)),
            "[white]R10[/white]": hex(mu.reg_read(UC_X86_REG_R10)),
            "[white]R11[/white]": hex(mu.reg_read(UC_X86_REG_R11)),
            "[white]R12[/white]": hex(mu.reg_read(UC_X86_REG_R12)),
            "[white]R13[/white]": hex(mu.reg_read(UC_X86_REG_R13)),
            "[white]R14[/white]": hex(mu.reg_read(UC_X86_REG_R14)),
            "[white]R15[/white]": hex(mu.reg_read(UC_X86_REG_R15)),
            "[grey50]EFLAGS[/grey50]": bin(mu.reg_read(UC_X86_REG_EFLAGS))
    }
    table = Table(box=box.ROUNDED) #title="Registers snapshot"
    table.add_column("Register")
    table.add_column("Value")
    for key, value in registers.items():
        table.add_row(key,value)
    return table

def hook_code_64(mu, address, size, user_data):
    global GO
    rip = mu.reg_read(UC_X86_REG_RIP)
    rip = mu.mem_read(rip,size)
            
    printable_rip = ''
    for b in rip:
        printable_rip += f'\\x{b:02x}'
    try:
        if rip != b'\x00\x00\x00\x00\x00\x00\x00\x00' and rip != b'\x00\x00\x00\x00' and rip != b'\x00\x00':
            cap = Cs(CS_ARCH_X86, CS_MODE_64)
            disasm = cap.disasm(rip,size)
            for d in disasm:
                if GO == False:
                    title = f'RIP @ [#826FFF]{hex(address)}[/#826FFF] : [bold green]{d.mnemonic} {escape(d.op_str)}[/bold green] ([italic white]{printable_rip}[/italic white])'
                    print(
                        Panel(
                            Columns([get_registers_64(mu), get_rsp(mu)], padding=(0,2)),
                            title=title,
                            title_align="left",
                            padding=(1,2)
                        )
                    )
                    x = input('Press <ENTER> to step forward or go to end by typing "g": ')
                    print('')
                    if 'g' in x: GO = True
        else:
            mu.emu_stop()
    
    except Exception as e:
        print(e)
    
    except KeyboardInterrupt:
        mu.emu_stop()
        print('[[red]![/red]] Emulation [red]stopped[/red]')

def emulate(payload, mode):
    #Reset global variable GO
    global GO
    GO = False
    #We want for example b'\x90\x90'. Direct .encode() or bytes() conversion here doesn't work because it results in e.g. b'\\x90\\x90'
    payload = str(payload)
    payload_bytes = b''
    ints = [int(b.strip().split('x')[1], 16) for b in re.findall(r'(\\x[a-f0-9]{2})', payload)]
    for i in ints:
        payload_bytes += pack('B',i)
    mu = init_mu(mode)
    address = 0x1000000
    mu.mem_map(address, 0x1000)
    mu.mem_write(address, payload_bytes)
    if mode == CS_MODE_32:
        mu.reg_write(UC_X86_REG_EIP, address)
    else:
        mu.reg_write(UC_X86_REG_RIP, address)
    start_address = address
    end_address = start_address + len(payload_bytes)
    try:
        if mode == CS_MODE_32:
            mu.hook_add(UC_HOOK_CODE, hook_code_32)
        else:
            mu.hook_add(UC_HOOK_CODE, hook_code_64)
        mu.emu_start(start_address, end_address)
        
        title = f'Emulation completed [green]successfully[/green]'
        if mode == CS_MODE_32:
           print(
                Panel(
                    Columns([get_registers_32(mu), get_esp(mu)], padding=(0,2)),
                    title=title,
                    title_align="left",
                    padding=(1,2)
                )
            )
        else: 
            print(
                Panel(
                    Columns([get_registers_64(mu), get_rsp(mu)], padding=(0,2)),
                    title=title,
                    title_align="left",
                    padding=(1,2)
                )
            )
    
    except UcError as e:
        print(f'[[red]![/red]] Error: {e} at [red]{hex(mu.reg_read(UC_X86_REG_EIP))}[/red]\n')
    
    except Exception as e:
        print(e)

if __name__ == '__main__':
    if sys.argv[1] == '32':
        mode = CS_MODE_32
    else:
        mode = CS_MODE_64
    emulate(sys.argv[2], mode)
