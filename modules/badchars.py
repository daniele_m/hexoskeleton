import re
from rich import print
from modules.utilities import wrap

#Generate badchars array as text or python code
def generate_badchars(excluded_hex):
    excluded_int = [int(b.strip().split('x')[1], 16) for b in re.findall(r'(\\x[a-fA-F0-9]{2})', excluded_hex)]
    return [f'\\x{i:02x}' for i in (set(range(0,256)) - set(excluded_int))]

#Display the list of badchars
def show_badchars(bad):
    print('Badchars: ', end='')
    if len(bad) > 0:
        for b in bad: print(f'[bold]{b}[/bold]', end='')
    else:
        print(None, end='')
    print('')

#Return True if badchars are found
def address_contains_badchars(text, bad):
    try:
        clean = text.split('x')[1]
        clean = wrap(clean, 2)
        for b in bad:
            if b.split('x')[1] in clean:
                return True
    except Exception as e:
        #print(e)
        raise()
    return False

#Color badchars in output strings
def color_badchars(text, badchars):
    for badchar in badchars:
        text = text.replace(f'{badchar}', f'[red]{badchar}[/red]')
    return text
