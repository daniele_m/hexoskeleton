import re
from capstone import Cs, CS_ARCH_X86, CS_MODE_32, CS_MODE_64
from keystone import Ks, KsError, KS_ARCH_X86, KS_MODE_32, KS_MODE_64
from rich import print
from rich.markup import escape
from modules.badchars import color_badchars
from modules.regex_list import REGEX_ASM_COMMENTS, REGEX_INVALID_CHARS

def assemble(code: str, arch, mode):
    code = re.sub(REGEX_INVALID_CHARS, '', code, flags=re.MULTILINE)
    try:
        ks = Ks(arch, mode)
        output, count = ks.asm(code)
        return output
    except Exception as e:
        print(f'{e}')
        raise()

def disassemble(shellcode: bytearray, cap_arch, cap_mode, badchars):
    try:
        cap = Cs(cap_arch, cap_mode)
        disasm = cap.disasm(shellcode, 0)
        output = ''

        for d in disasm:
            current_op_bytes = ''.join([f'\\x{b:02x}' for b in d.bytes])
            colored = color_badchars(current_op_bytes, badchars)
            formatted_output  = f'[bold green]{hex(d.address + 0x10000000) :<4}[/bold green] : '
            formatted_output += f'{colored :<{(26 + len(colored) - len(current_op_bytes) if cap_mode == CS_MODE_32 else 42 + len(colored) - len(current_op_bytes))}} | '
            formatted_output += f'{d.mnemonic} [italic white]{escape(d.op_str)}[/italic white]\n'
            output += formatted_output

        return output
        
    except:
        raise()
        print(f'[[red]![/red]] Cannot disassemble')
