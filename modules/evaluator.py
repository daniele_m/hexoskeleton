import re
from struct import pack, unpack
from modules.output_formatter import print_evaluator
from modules.utilities import *
from modules.regex_list import REGEX_FORMATS, REGEX_FIRST_NEG, REGEX_OPERANDS
from keystone import KS_MODE_32, KS_MODE_64

def evaluate(data, mode, debug):
    matches = re.findall(REGEX_OPERANDS, data)
    
    if re.match(REGEX_FIRST_NEG,data):
        result = 'int(0-'
    else:
        result = 'int(0+'

    for match in matches:
        #Store value to temp variable
        operand, operator = match[0].strip(), match[1].strip()

        #Remove leading zeroes and leading 0n/0d/0x
        try:
            if operand[0] == '0' and operand[1] == '0':
                while operand[0] == '0': operand = operand[1:]
        except:
            pass
        if operand.startswith('0n'): 
            operand = operand.split('0n')[1]
        elif operand.startswith('0d'):
            operand = operand.split('0d')[1]
        elif re.match(r'[a-zA-Z]+', operand):
            operand = f'0x{operand}'

        #Add value to eval string
        result += str(operand)

        #Check sign - keep the one on the right if there's more than 1
        if len(operator) >= 1:
            #Add it to the eval string
            result += operator[-1]
        else:
            break

    #If the eval string ends with a symbol - delete it
    result = result.rstrip()
    while result[-1] in ['+', '-', '*', '/', '^']:
        result = result[:-1].rstrip()

    #Use a logical and with the final result to hide overflows and underflows
    if mode == KS_MODE_32:
        result += ') & ((1<<32)-1)'
    else:
        result += ') & ((1<<64)-1)'
    
    #Debug output before evaluation
    if debug: print(f'[grey50][DEBUG][/grey50] Expression: [italic]{escape(result)}[/italic]\n')

    try:
        #Call eval() on the eval string we built        
        result = eval(result)
        
    except Exception as e:
        print(f'[[red]![/red]] Invalid expression: {e}')
    
    print_evaluator(result)
