import string
from pwnlib.util.cyclic import cyclic_gen

#Pattern's alphabet defaults to uppercase + lowercase + digits
ALPHABET = string.ascii_uppercase + string.ascii_lowercase + string.digits

#Pattern create command
def pattern_create(size: int):
    g = cyclic_gen(n=4, alphabet=ALPHABET)
    return g.get(size)

#Pattern offset command
def pattern_find(size: int, eip: str):
    g = cyclic_gen(n=4, alphabet=ALPHABET).get(size)
    found = g.find(eip)
    assert found >= 0
    return found
