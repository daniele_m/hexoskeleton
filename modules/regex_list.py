#Regex for evaluator operands: 0x prefixed digits / 0n prefixed digits / 0d prefixed digits / digits only / letters only / common operators (+, -, *, /)
REGEX_OPERANDS = r'(0x[\da-fA-F]{1,16}|0[nd][\d]{1,16}|[\d]{1,16}(?![a-zA-Z]+)|[a-fA-F0-9]{1,16})[ ]*([\+\-\*\/\^]*)[ ]*'

#Regex for formats: 0x prefixed digits / 0n prefixed digits / digits only
REGEX_FORMATS = r'((0[xn])*[\da-fA-F]{1,16})' 

#Regex used to check if first operand is negative?
REGEX_FIRST_NEG = r'[ ]*-'

#Regex to identify and remove all asm comments
REGEX_ASM_COMMENTS = r'(a-zA-Z0-9\.\?\[\]\,\+\:\/\*\r\n\t\ )*;.*'

#Regex to identify and remove all invalid characters
REGEX_INVALID_CHARS = r'[^a-zA-Z0-9:;+,\*\-\.\[\]\r\n\t ]'
