import numpy
from blake3 import blake3

def ror_hash(byte, count):
    #ROR hashing (4 bytes)
    binb = numpy.base_repr(byte, 2).zfill(32)
    while count > 0:
        binb = binb[-1] + binb[0:-1]
        count -= 1
    return (int(binb, 2))

def blake3_hash(byte):
    #Blake3 hashing (8 bytes)
    return '0x'+blake3(byte).hexdigest(length=8)
