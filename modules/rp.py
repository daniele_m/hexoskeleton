import os
import re
import pathlib
import subprocess
from modules.badchars import address_contains_badchars
from rich import print
from rich.markup import escape

#rp++ options
RP_CMD = '{rp} -f {target} -r {size} {extra} > {output}'
RP_PATH = './rp++'
RP_SIZE = '5'
RP_PARSED = './rp++.log'
RP_XTRA = '' #Extra args (unused)

#Remove rp++ output file's color and `found` text
def remove_nasty_chars(text):
    for nasty in ['[91m', '[0m', '[92m', '(1 found)']:
        text = text.replace(nasty, '')
    return text

def execute_rp(target, output):
    try:
        #Added check to the output path in order not to overwrite the input binary
        assert output != target
        if pathlib.Path(output).expanduser().is_file():
            try:
                #If the output is not the input binary, overwrite it
                os.remove(pathlib.Path(output).expanduser())
            except Exception as e:
                print(e)
                return False
        
        if pathlib.Path(target).expanduser().is_file():
            cmd = RP_CMD.format(
                rp = RP_PATH,
                target = target,
                size = RP_SIZE,
                extra = RP_XTRA,
                output = output
            )
            rp_status = subprocess.call(cmd, shell=True)
            #Added additional check because sometimes rp++ doesnt end with exit code != 0 but outputs critical errors
            assert rp_status == 0 and not ('[EXCEPTION REPORT]:' in pathlib.Path(output).expanduser().read_text())
            return True
    except AssertionError:
        print(f'\n[[red]![/red]] Output file cannot and input file cannot be the same')
        return False
    except:
        print(f'\n[[red]![/red]] Unknown error')
        return False

#Parse rp++ output file and return address + asm code in a dict
def parse_rp(target, badchars):
    parsed = {}
    
    try:
        content = pathlib.Path(target).expanduser().read_text(encoding='utf-8').split('\n')
    except:
        content = pathlib.Path(target).expanduser().read_text(encoding='utf-16').split('\n')
    
    #Initial cleanup to remove colors and extra spaces
    content = [remove_nasty_chars(x).strip() for x in content]
    
    #Additional checks
    content = [x for x in content if(
        #We don't want gadgets with length 18 because those are simple 'ret ;'
            len(x) > 18
        ) and (
            #They must contain ret or retn
            'ret  ;' in x or 'retn  ;' in x or re.match('ret[n]{0,1} 0x[0-9a-fA-F]{2}', x)
        ) and (
            #We don't want jump or leave instruction
            'jmp' not in x and 'leave' not in x
        ) and (
            #We don't want values bigger than 0xff in square brackets
            len(re.findall(r'0x[a-zA-Z0-9]{3,8}\]', x.split(':',1)[1])) == 0
        )]

    for line in content:
        #Split line at ':', then store address [0] and code [1]
        address = line.split(':',1)[0].strip()
        code = line.split(':',1)[1].strip()
        #Then filter out addresses that contain badchars
        if address_contains_badchars(address, badchars) == False:
            parsed[address] = code
    
    print(f'Collected [white]{len(parsed)}[/white] gadgets')
    if len(parsed) == 0:
        print(f'    Try removing \\x00 from badchars first')
    return parsed

#Print ROP gadgets stored in global variable
def find_gadget(gadgets, regex, badchars):
    #Default to "all" (.*) if regex string is empty
    if not (len(regex) > 1 and regex.strip() != ''): regex = '.*'
    try:
        if len(gadgets) > 0:
            for address, code in gadgets.items():
                #Filter out addresses that contain badchars
                if address_contains_badchars(address, badchars) == False:
                    if re.match(regex, code):
                        final_code = ''
                        code = code.replace('[', r'\[')
                        code = [c.strip().split(' ') for c in code.strip().split(';') if c != '']
                        for line in code:
                            line[0] = f'[bold green]{escape(line[0])}[/bold green]'
                            line = ' '.join(line)
                            final_code += line + ' [grey50];[/grey50] '
                        print(f'[#826FFF]{address}[/#826FFF][grey50]:[/grey50] [white]{final_code}[/white]')
        else:
            print(f'[[red]![/red]] You first have to parse rp++ output using [red]rop_parse[/red] or [red]rop_collect[/red]')
    except: pass
