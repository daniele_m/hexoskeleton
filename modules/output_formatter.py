import re
from rich import print
from rich.markup import escape
from modules.utilities import stack_alignment_check, wrap
from modules.regex_list import REGEX_FORMATS
from modules.badchars import color_badchars
import string

#WinDbg's "?" / evaluator format string
WDBG_OUTPUT_FORMAT = 'Result: [#826FFF]{dec}[/#826FFF] = [bold #e0e0e0]{hex}[/bold #e0e0e0]'

#Print output in WinDbg's "?" / evaluator style
def print_evaluator(value: int):
    try:
        print(WDBG_OUTPUT_FORMAT.format(dec = value, hex = hex(value)))
        stack_alignment_check(value)
    except Exception as e:
        # print(e)
        pass

#Print badchars array
def print_badchars_array(badchars, as_python=False):
    if as_python == True:
        badchars = wrap(badchars, 40)
        for idx,line in enumerate(badchars):
            if(idx == 0):
                print('badchars  = b"', end='')
            else:
                print('badchars += b"', end='')
            print(line + '"')
    else:
        print(f'Badchars:\n{badchars}')

#Print shellcode as text or python code
def print_shellcode(shellcode: bytearray, badchars, as_python=True):
    #Get shellcode length
    try: length = len(shellcode)
    except: length = 0

    if length > 0:
        #Convert to shellcode string
        shellcode_string = ''.join([f'\\x{b:02x}' for b in shellcode])
        print(f'Size: [bold white]{len(shellcode)}[/bold white] bytes')
        
        #Print in Python format
        if as_python == True:
            shellcode_string = wrap(shellcode_string, 40)
            for idx,line in enumerate(shellcode_string):
                if(idx == 0):
                    print('shellcode  = b"', end='')
                else:
                    print('shellcode += b"', end='')               
                line = color_badchars(line, badchars)
                print(line + '"')
        #And raw string format
        else:
            shellcode_string = color_badchars(shellcode_string, badchars)
            print(f'Shellcode: {shellcode_string}')
    else:
        print(f'Shellcode: {None}')
