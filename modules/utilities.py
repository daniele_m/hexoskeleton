import re
import codecs
from rich import print
from rich.markup import escape
from struct import pack, unpack
from modules.regex_list import REGEX_FORMATS
from ipaddress import ip_address
from keystone import KS_ARCH_X86, KS_MODE_32, KS_MODE_64

#Simple check to verify that a value is an IP address
def is_ipaddress(value):
    try:
        ip_address(value)
        return True
    except:
        return False

#Check if value is divisible by 4 and 8
def stack_alignment_check(value):
    display_32 = '32bit'
    display_64 = '64bit'
    print(f'''Alignment: {
        f"[red]{display_32}[/red]" if not value % 0x04 == 0 and value != 0 else f"[green]{display_32}[/green]"
    }, {
        f"[red]{display_64}[/red]" if not value % 0x10 == 0 and value != 0 else f"[green]{display_64}[/green]"
    }''')

#Convert int to stack-pushable
def int_to_stack_pushable(value):
    integer = int(value, 10)
    if integer != 0:
        packed = pack('<L', integer)
        final = ''
        for b in packed:
            final += f'{b:02x}'
        final = final.replace('0000', '') #?
        final = final.replace('00', '')   #??
    else:
        final = '00'
    return final

def wrap(s, n):
    result = []
    temp = ""

    for char in s:
        temp += char
        if len(temp) == n:
            result.append(temp)
            temp = ""

    if temp:
        result.append(temp)

    return result

#Convert int/string to hex e.g. for passing string arguments to functions on the stack (ANSI)
def to_stack_pushable_a(value, mode):
    #IP address regex, to auto-split the s)tring on dots
    if is_ipaddress(value):
        print(f'[white]Input string is an IP address: 0x{"".join(int_to_stack_pushable(i) for i in value.split(".")[::-1])}[/white]')
    else:
        # try:
            # final = int_to_stack_pushable(value)
            # print(f'[white]Input string is an int number: 0x{final}[/white]')
        # except:
            # pass
            
            group_by = 4 if mode == KS_MODE_32 else 8
            value = str(value)
            # Null termination
            if (len(value) % group_by) == 0:
                print(f'[white]xor eax, eax\npush eax[/white]') if mode == KS_MODE_32 else print(f'[white]xor rax, rax\npush rax[/white]')
            # The actual ansi string
            for x in wrap(value, group_by)[::-1]:
                x = x[::-1]
                encoded = x.encode('utf-8')
                final = (codecs.encode(encoded, 'hex')).decode('utf-8')
                    
                if mode == KS_MODE_32:
                    print(f'[white]push 0x{final}[/white]')
                else:
                    print(f'[white]mov rax, 0x{final}[/white]')
                    print(f'[white]push rax[/white]')

#Convert int/string to hex e.g. for passing string arguments to functions on the stack (UNICODE)
def to_stack_pushable_w(value, mode):
    try:
        final = int_to_stack_pushable(value)
        print(f'[white]Input string is an int number: 0x{final}[/white]')
    except:
        pass
        
        group_by = 4 if mode == KS_MODE_32 else 8
        value = str(value)
        newvalue = ''.join([f'{v}\0' for v in value])
        
        # Null termination
        if (len(newvalue) % group_by) == 0:
            print(f'[white]xor eax, eax\npush eax[/white]') if mode == KS_MODE_32 else print(f'[white]xor rax, rax\npush rax[/white]')
        # The actual unicode string
        for x in wrap(newvalue, group_by)[::-1]:
            x = x[::-1]
            encoded = x.encode('utf-8')
            final = (codecs.encode(encoded, 'hex')).decode('utf-8')

            if mode == KS_MODE_32:
                print(f'[white]push 0x{final}[/white]')
            else:
                print(f'[white]mov rax, 0x{final}[/white]')
                print(f'[white]push rax[/white]')

#Swap endianness of a hex value provided as address or shellcode
def swap_endianness(value):
    if re.match(r'0x[a-fA-F0-9]{2,16}', value):
        value = value.split('x')[1]
    elif re.match(r'(\\x[a-fA-F0-9]{2})+', value):
        value = re.sub(r'\\x','',value)

    while len(value) % 4 != 0 or len(value) % 8 != 0:
        value = '0' + value
    try:
        #Reverse it
        value = wrap(value, 2)[::-1]

        #Address/Hex format
        if len(value) <= 8:
            print(f'Hexaddress: [white bold]0x{"".join(value)}[/white bold]')
        
        #Shellcode format
        print(f'Shellcode:  [white bold]{"".join(['\\x' + v for v in value])}[/white bold]')
    except Exception as e:
        print(e)
