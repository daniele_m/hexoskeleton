#!/usr/bin/env python3
import re
import os
import sys
import cmd
import flask
import pathlib
import textwrap
import argparse
from rich import print
from rich.markup import escape
from struct import pack
from capstone import CS_ARCH_X86, CS_MODE_32, CS_MODE_64
from keystone import KS_ARCH_X86, KS_MODE_32, KS_MODE_64, Ks

from modules.evaluator import evaluate
from modules.pattern import pattern_create, pattern_find
from modules.output_formatter import print_badchars_array, print_shellcode
from modules.hashing import ror_hash, blake3_hash
from modules.badchars import generate_badchars, show_badchars, address_contains_badchars, color_badchars
from modules.rp import execute_rp, parse_rp, find_gadget
from modules.utilities import is_ipaddress, to_stack_pushable_a, to_stack_pushable_w, swap_endianness
from modules.assembler import assemble, disassemble
from modules.regex_list import REGEX_ASM_COMMENTS
from modules.x86_emulator import emulate

#--- Global variables ---
DEBUG = False
KNOWN_BAD = []
SHELLCODE = b''
ROP_GADGETS = ''
OUT_AS_PYTHON = False
ARCH = KS_ARCH_X86
MODE = KS_MODE_32
CAP_ARCH = CS_ARCH_X86
CAP_MODE = CS_MODE_32

app = flask.Flask('shellcode')
@app.route('/')
def main():
    return flask.Response(bytes(SHELLCODE), mimetype='application/octet-stream')

class Cli(cmd.Cmd):
    prompt = f'\033[01m\033[33mhexo\033[0m> '
    intro = f'\n/ Hexoskeleton /\nA hard outer layer that covers, supports, and protects the body of an invertebrate animal.\nOne without a line of bones in its back, such as an exploit developer.\n'

    def do_toggle_debug(self, line):
        """Toggle debug output on/off"""
        global DEBUG
        DEBUG = not DEBUG
        print(f'Debug output: {"[red]Off[/red]" if not DEBUG else "[green]On[/green]"}')

    def do_toggle_python(self, line):
        """Toggle output as python code on/off"""
        global OUT_AS_PYTHON
        OUT_AS_PYTHON = not OUT_AS_PYTHON
        print(f'Output in Python format: {"[red]Off[/red]" if not OUT_AS_PYTHON else "[green]On[/green]"}')

    def do_toggle_64bit(self, line):
        """Toggle 32/64 bit mode"""
        global MODE
        global CAP_MODE
        if MODE == KS_MODE_32:
            MODE = KS_MODE_64
            CAP_MODE = CS_MODE_64
        else:
            MODE = KS_MODE_32
            CAP_MODE = CS_MODE_32

        print(f'Switched to: {"[white bold]64bit[/white bold]" if MODE == KS_MODE_64 else "[white bold]32bit[/white bold]"}')

    def do_http(self, line):
        """Serve the shellcode saved in the global variable over HTTP (0.0.0.0:9000)"""
        app.run(host='0.0.0.0', port='9000', debug=False, use_reloader=False)

    def do_exit(self, line):
        """Exit the script"""
        return True

    def do_exec(self, line):
        """Execute the os command passed as argument.\nExample:\n  exec <commands>\n  exec ls -hal"""
        if line == '':
            print('Empty argument. Type "help exec" for more info.')
            return
        os.system(line.strip())

    def do_clear(self, line):
        """Clear the screen"""
        if os.name == 'posix':
            os.system('clear')
        else:
            os.system('cls')

    def do_calc(self, line):
        """Perform operations between hexadecimal/decimal values like WinDbg's ? command.\nIf any input value is lacking a prefix (0x for hexadecimal or 0n/0d for decimal), then the value will be treated like hexadecimal by default.\nExample:\n  calc <expression>\n  calc 0n2846652848 - 0xcafebabe - 0d3\n  calc 0x00000000ffffffff + 0x65\n  calc 400 + 0n200 - a00 + a60 + 0d17\n  """
        if line == '':
            print('Empty argument. Type "help calc" for more info.')
            return
        evaluate(line, MODE, DEBUG)
    
    def do_badchars(self, line):
        """Show or set the known badchars. Pass none as an argument to unset all badchars.\nExample:\n  badchars\n  badchars \\x00\\0xda\\0xff\\0x0a\n  badchars none"""
        global KNOWN_BAD
        if line.lower() == 'none':
            KNOWN_BAD = []
        else:
            parsed = [b.strip() for b in re.findall(r'(\\x[a-fA-F0-9]{2})', line)]
            if len(parsed) > 0:
                KNOWN_BAD = parsed
        show_badchars(KNOWN_BAD)

    def do_gen_badchars(self, line):
        """Return the badchar array after removing the user-provided bytes.\nExample:\n  gen_badchars <known badchars>\n  gen_badchars \\x00\\x0a\\0x20\\0xff\n  gen_badchars"""
        badchars = ''.join(generate_badchars(''.join(KNOWN_BAD) + line))
        print_badchars_array(badchars, OUT_AS_PYTHON)
        
    def do_rop_collect(self, line):
        """Execute rp++ against target binary and collect ROP gadgets into hexoskeleton.\nThe input binary is provided as first argument, while the output log is saved to the path provided as second argument.\nExample:\n  rop_collect <binary file> <output logfile>\n  rop_collect target.exe 1.log"""
        if line == '':
            print('Empty argument. Type "help rop_collect" for more info.')
            return
        global ROP_GADGETS
        try:
            binary = line.split(' ',1)[0]
            output = line.split(' ',1)[1]
            print(f'[[green]*[/green]] [white]Launching rp++ against {escape(binary)}... [/white]')
            if execute_rp(binary, output) == True:
                print(f'Output saved to [white bold]{output}[/white bold]')
                ROP_GADGETS = parse_rp(output, KNOWN_BAD)
        except:
            print(f'[[red]![/red]] Cannot execute rp++')

    def do_rop_parse(self, line):
        """Parse an existing rp++ log file into hexoskeleton.\nExample:\n  rop_parse <logfile path>\n  rop_parse 1.log"""
        if line == '':
            print('Empty argument. Type "help rop_parse" for more info.')
            return
        global ROP_GADGETS
        try:
            binary = line
            if pathlib.Path(binary).expanduser().is_file():
                print(f'[[green]*[/green]] Parsing rp++ output {escape(binary)}[white]...[/white] ', end='')
                ROP_GADGETS = parse_rp(binary, KNOWN_BAD)
            else:
                print(f'[[red]![/red]] [red]{escape(binary)}[/red] isn\'t a file')
        except:
            print(f'[[red]![/red]] Cannot open file [red]{escape(binary)}[/red]')

    def do_rop_find(self, line):
        """Find ROP gadgets in parsed rp++ log by using python regular expressions.\nAll addresses that contain known badchars are filtered out automatically.\nExample:\n  rop_find <regex>\n  rop_find push ecx.*pop\n  rop_find mov dword \\[esi\\]\n  rop_find xor [a-z]{3}, [a-z]{3} ; ret\n  rop_find (?!.*(les|adc|ebp|shr|sbb|aam|\\[)).*push e[abc]x.*pop e[sd]i"""
        global ROP_GADGETS
        find_gadget(ROP_GADGETS, line, KNOWN_BAD)


    def do_asm(self, line):
        """Assemble a file or the raw asm instructions passed as argument.\nThe output shellcode is saved as global variable.\nExample:\n  asm <raw instructions or file path>\n  asm xor eax, eax; inc eax; push eax; pop ecx\n  asm ./code.asm"""
        if line == '':
            print('Empty argument. Type "help asm" for more info.')
            return
        global SHELLCODE
        try:
            if pathlib.Path(line).expanduser().is_file():
                line = pathlib.Path(line).expanduser().read_text()
                line = re.sub(REGEX_ASM_COMMENTS, r'\1', line, flags=re.MULTILINE)
            else:
                if ';' != line.rstrip()[-1]: line += ';'

            SHELLCODE = assemble(line, ARCH, MODE)
                
            if DEBUG:
                print('[grey50][DEBUG][/grey50] Disassembled code:')
                try:
                    print(disassemble(bytearray(SHELLCODE), CAP_ARCH, CAP_MODE, KNOWN_BAD))
                except Exception as e:
                    print(e)
            
            print_shellcode(SHELLCODE, KNOWN_BAD, OUT_AS_PYTHON)

        except:
            print(f'[[red]![/red]] Cannot assemble. Check your syntax.')

    def do_disasm(self, line):
        """Disassemble the shellcode saved in global variable or passed as argument.\nExample:\n  disasm\n  disasm \\x00\\x00\\x00\\x00"""
        global SHELLCODE
        if len(SHELLCODE) > 0 and len(line) == 0:
            shellcode_bytes = b''
            for b in SHELLCODE:
                shellcode_bytes += pack('B', b)
            try:
                print(disassemble(bytearray(SHELLCODE), CAP_ARCH, CAP_MODE, KNOWN_BAD))
            except Exception as e:
                print(e)
        elif len(line) > 0:
            if '\\x' in line:
                shellcode_bytes = b''
                ints = [int(b.strip().split('x')[1], 16) for b in re.findall(r'(\\x[a-fA-F0-9]{2})', line)]
                for i in ints:
                    shellcode_bytes += pack('B', i)
                try:
                    print(disassemble(bytearray(shellcode_bytes), CAP_ARCH, CAP_MODE, KNOWN_BAD))
                except Exception as e:
                    print(e)
        else:
            print(f'[[red]![/red]] Pass some shellcode as an argument, or create it using the [green]asm[/green] command')

    def do_show_shellcode(self, line):
        """Print the shellcode saved as global variable.\nExample:\n  show_shellcode"""
        print_shellcode(SHELLCODE, KNOWN_BAD)

    def do_hash_ror(self, line):
        """Calculate the 4 bytes ror hash of the string provided as argument.\nExample:\n  hash_ror <string>\n  hash_ror WriteProcessMemory\n  hash_ror WinExec"""
        if line == '':
            print('Empty argument. Type "help hash_ror" for more info.')
            return
        esi = line.strip()
        edx = 0x00
        ror_count = 0
        for eax in esi:
            edx = edx + ord(eax)
            if ror_count < len(esi)-1:
                edx = ror_hash(edx, 0xd)
            ror_count += 1
        print(f'[bold #e0e0e0]{hex(edx)}[/bold #e0e0e0]')

    def do_hash_blake3(self, line):
        """Calculate the 8 bytes blake3 hash of the string provided as argument.\nExample:\n  hash_blake3 <string>\n  hash_blake3 WriteProcessMemory\n  hash_blake3 WinExec"""
        if line == '':
            print('Empty argument. Type "help hash_blake3" for more info.')
            return
        line = line.strip()
        blake = blake3_hash(line.encode())
        print(f'[bold #e0e0e0]{blake}[/bold #e0e0e0]')

    def do_stack_pushable_a(self, line):
        """Convert string, int or IP address to stack pushable values (ANSI).\nExample:\n  stack_pushable_a <string>\n  stack_pushable_a c:\\windows\\system32\\calc.exe\n  stack_pushable_a 1.2.3.4\n  stack_pushable_a 1337"""
        if line == '':
            print('Empty argument. Type "help stack_pushable_a" for more info.')
            return
        to_stack_pushable_a(line, MODE)
    
    def do_stack_pushable_w(self, line):
        """Convert string, int or IP address to stack pushable values (UNICODE).\nExample:\n  stack_pushable_w <string>\n  stack_pushable_w c:\\windows\\system32\\calc.exe\n  stack_pushable_w 1.2.3.4\n  stack_pushable_w 1337"""
        if line == '':
            print('Empty argument. Type "help stack_pushable_w" for more info.')
            return
        to_stack_pushable_w(line, MODE)

    def do_swap_endianness(self, line):
        """Swap endianness of the hex address or shellcode provided as argument.\nExample:\n  swap_endianness <hex address or shellcode>\n  swap_endianness 0xaabbccdd\n  swap_endianness \\x00\\x11\\x22\\x33"""
        if line == '':
            print('Empty argument. Type "help swap_endianness" for more info.')
            return
        swap_endianness(line)

    def do_pattern_create(self, line):
        """Create chunks of de Bruijn sequences using pwntools.\nExample:\n  pattern_create <length>\n  pattern_create 1000"""
        if line == '':
            print('Empty argument. Type "help pattern_create" for more info.')
            return
        try:
            pattern = pattern_create(int(line))
            if OUT_AS_PYTHON == True:
                pattern = textwrap.wrap(pattern, 40)
                for idx,line in enumerate(pattern):
                    if(idx == 0):
                        print('pattern  = b"', end='')
                    else:
                        print('pattern += b"', end='')
                    print(line + '"')
            else:
                print(pattern)
        except:
            print(f'[[red]![/red]] Invalid input')

    def do_pattern_offset(self, line):
        """Find the offset to the provided value (as displayed in memory, prefixed by "0x") in the generated de Bruijn sequence.\nExample:\n  pattern_offset <length> <value>\n  pattern_offset 1000 0x45414146"""
        if line == '':
            print('Empty argument. Type "help pattern_offset" for more info.')
            return
        try:
            size = line.split(' ',1)[0]
            eip = (line.split(' ',1)[1])[-8:]                
            eip = textwrap.wrap(eip,2)[::-1]
            final = ''
            for x in eip:
                final += (bytes.fromhex(x)).decode()
            assert(len(final) % 4 == 0 or len(final) % 8 == 0)
            offset = pattern_find(int(size),str(final))
            print(f'[[green]*[/green]] Pattern found at offset [white bold]{offset}[/white bold]')
        except:
            print(f'[[red]![/red]] Invalid input')    

    def do_emulate(self, line):
        """Emulate basic assembly sequences (no support for OS functions).\nIf no argument is provided, the shellcode is taken from the global variable.\nExample:\n  emulate <shellcode or file>\n  emulate \\x30\\xc0\\xfe\\xc0\n  emulate code.asm\n  emulate"""
        global SHELLCODE
        if len(line) == 0 and len(SHELLCODE) != 0:
            try:
                shellcode = ''
                for b in SHELLCODE:
                    shellcode += f'\\x{b:02x}'
                emulate(shellcode, CAP_MODE)
                print('')
            except:
                print(f'[[red]![/red]] Unknown error')
        else:
            if DEBUG and len(line) > 0 : print(f'[grey50][DEBUG][/grey50] Trying to open file [italic][white]{escape(line)}[/white][/italic]\n')
            try:
                code = pathlib.Path(line).expanduser().read_text()
                code = re.sub(REGEX_ASM_COMMENTS, r'\1', code, flags=re.MULTILINE)

                if DEBUG: print(f'[grey50][DEBUG][/grey50] Code:\n[italic]{escape(code)}[/italic]\n')
                ks = Ks(ARCH, MODE)
                output, count = ks.asm(code)
                shellcode = ''
                for b in output:
                    shellcode += f'\\x{b:02x}'
                emulate(shellcode, CAP_MODE)
            except:
                try:
                    assert ('\\x' in line) == True
                    print(f'[[green]*[/green]] Treating input as shellcode[white]...[/white]')
                    emulate(line, CAP_MODE)
                except:
                    print(f'[[red]![/red]] Invalid shellcode')

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("-d", "--debug", help="toggle debug text", action="store_true", default=False, required=False)
    parser.add_argument("-b", "--badchars", metavar='"\\x00\\0x0a"', help="start with the provided set of badchars", default='', required=False)
    parser.add_argument("-n", "--bitness", choices=['32', '64'], metavar='32/64', help="32 or 64 bit", default='32', required=False)
    parser.add_argument("-c", "--commands", metavar='"asm jmp +0x10"', help="list of one-shot commands", default=[], action='append', required=False)
    parser.add_argument("-p", "--python", help="toggle output as python code", action='store_true', default=False, required=False)
    args = parser.parse_args()

    if args.debug: DEBUG = args.debug
    if args.python: OUT_AS_PYTHON = args.python
    if args.badchars: KNOWN_BAD = [b.strip() for b in re.findall(r'(\\x[a-fA-F0-9]{2})', args.badchars)]
    if args.bitness == '32':
        MODE = KS_MODE_32
        CAP_MODE = CS_MODE_32
    else:
        MODE = KS_MODE_64
        CAP_MODE = CS_MODE_64
    
    if len(args.commands) > 0:
        for c in args.commands:
            Cli().onecmd(c)
            print('')
    else:
        Cli().cmdloop()
