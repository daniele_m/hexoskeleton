# Hexoskeleton

<img src="images/cover.png" width="90%">

A custom tool that helped me prepare for OSED. Uses [rp++](https://github.com/0vercl0k/rp/) to search for ROP gadgets.

It provides a self-contained environment for building and testing shellcode, including the ability to serve it via HTTP for remote execution (e.g. with the `companion.py` script).

Tested with Python 3.12 on Linux.

